package com.gerejadibandung.minuteswiththelord;

/**
 * @author yacob
 *
 */
public class TextList {

	public String start;
	public String title;
	public String exit;
	public String calling;
	public String calling_desc;
	public String praying;
	public String praying_desc;
	public String pray_reading;
	public String pray_reading_desc;
	public String confession;
	public String confession_desc;
	public String consecration;
	public String consecration_desc;
	public String thanksgiving;
	public String thanksgiving_desc;
	public String petition;
	public String petition_desc;
	public String the_end;
	public String halelujah;
	public String reset;
	public String stop;
	public String pause;
	public String unpause;
	public String alert_message1;
	public String alert_message2;
	public String alert_message3;
	public String number_7;
	public String number_65;
	public String number_55;
	public String number_3;
	public String number_2;
	public String number_15;
	public String number_1;
	
	public String getStart() {
		return start;
	}
	public void setStart(String start) {
		this.start = start;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getExit() {
		return exit;
	}
	public void setExit(String exit) {
		this.exit = exit;
	}
	public String getCalling() {
		return calling;
	}
	public void setCalling(String calling) {
		this.calling = calling;
	}
	public String getCalling_desc() {
		return calling_desc;
	}
	public void setCalling_desc(String calling_desc) {
		this.calling_desc = calling_desc;
	}
	public String getPraying() {
		return praying;
	}
	public void setPraying(String praying) {
		this.praying = praying;
	}
	public String getPraying_desc() {
		return praying_desc;
	}
	public void setPraying_desc(String praying_desc) {
		this.praying_desc = praying_desc;
	}
	public String getPray_reading() {
		return pray_reading;
	}
	public void setPray_reading(String pray_reading) {
		this.pray_reading = pray_reading;
	}
	public String getPray_reading_desc() {
		return pray_reading_desc;
	}
	public void setPray_reading_desc(String pray_reading_desc) {
		this.pray_reading_desc = pray_reading_desc;
	}
	public String getConfession() {
		return confession;
	}
	public void setConfession(String confession) {
		this.confession = confession;
	}
	public String getConfession_desc() {
		return confession_desc;
	}
	public void setConfession_desc(String confession_desc) {
		this.confession_desc = confession_desc;
	}
	public String getConsecration() {
		return consecration;
	}
	public void setConsecration(String consecration) {
		this.consecration = consecration;
	}
	public String getConsecration_desc() {
		return consecration_desc;
	}
	public void setConsecration_desc(String consecration_desc) {
		this.consecration_desc = consecration_desc;
	}
	public String getThanksgiving() {
		return thanksgiving;
	}
	public void setThanksgiving(String thanksgiving) {
		this.thanksgiving = thanksgiving;
	}
	public String getThanksgiving_desc() {
		return thanksgiving_desc;
	}
	public void setThanksgiving_desc(String thanksgiving_desc) {
		this.thanksgiving_desc = thanksgiving_desc;
	}
	public String getPetition() {
		return petition;
	}
	public void setPetition(String petition) {
		this.petition = petition;
	}
	public String getPetition_desc() {
		return petition_desc;
	}
	public void setPetition_desc(String petition_desc) {
		this.petition_desc = petition_desc;
	}
	public String getThe_end() {
		return the_end;
	}
	public void setThe_end(String the_end) {
		this.the_end = the_end;
	}
	public String getHalelujah() {
		return halelujah;
	}
	public void setHalelujah(String halelujah) {
		this.halelujah = halelujah;
	}
	public String getReset() {
		return reset;
	}
	public void setReset(String reset) {
		this.reset = reset;
	}
	public String getStop() {
		return stop;
	}
	public void setStop(String stop) {
		this.stop = stop;
	}
	public String getPause() {
		return pause;
	}
	public void setPause(String pause) {
		this.pause = pause;
	}
	public String getUnpause() {
		return unpause;
	}
	public void setUnpause(String unpause) {
		this.unpause = unpause;
	}
	public String getAlert_message1() {
		return alert_message1;
	}
	public void setAlert_message1(String alert_message1) {
		this.alert_message1 = alert_message1;
	}
	public String getAlert_message2() {
		return alert_message2;
	}
	public void setAlert_message2(String alert_message2) {
		this.alert_message2 = alert_message2;
	}
	public String getAlert_message3() {
		return alert_message3;
	}
	public void setAlert_message3(String alert_message3) {
		this.alert_message3 = alert_message3;
	}
	public String getNumber_7() {
		return number_7;
	}
	public void setNumber_7(String number_7) {
		this.number_7 = number_7;
	}
	public String getNumber_65() {
		return number_65;
	}
	public void setNumber_65(String number_65) {
		this.number_65 = number_65;
	}
	public String getNumber_55() {
		return number_55;
	}
	public void setNumber_55(String number_55) {
		this.number_55 = number_55;
	}
	public String getNumber_3() {
		return number_3;
	}
	public void setNumber_3(String number_3) {
		this.number_3 = number_3;
	}
	public String getNumber_2() {
		return number_2;
	}
	public void setNumber_2(String number_2) {
		this.number_2 = number_2;
	}
	public String getNumber_15() {
		return number_15;
	}
	public void setNumber_15(String number_15) {
		this.number_15 = number_15;
	}
	public String getNumber_1() {
		return number_1;
	}
	public void setNumber_1(String number_1) {
		this.number_1 = number_1;
	}
	
	
}
